﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivateChildren : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {


    }

    void OnTriggerEnter(Collider other)
    {
        foreach (Transform child in transform)
        {
            child.gameObject.SetActive(true);
            child.gameObject.GetComponent<BoxCollider>().enabled = true;
        }
    }
}
