﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class WeaponObject : ScriptableObject {

    public string weaponName = "Weapon Name Here";
    public int cost = 50;
    public string description;

    public float fireRate = .5f;
    public int damage = 10;
    public float range = 100f;

    public Material gunMaterial;
    //public GameObject gun;


    void Awake()
    {
        // Change current weapon material to the specific gun's material.
        //childObject.transform.parent.gameObject.getComponent("Material") = gunMaterial;
    }
}
