﻿using UnityEngine;
using System.Collections;

public class ShopController : MonoBehaviour
{

    public GameObject shopCanvas;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
            OpenShop();

    }

    void OpenShop()
    {
        shopCanvas.SetActive(true);
        Time.timeScale = 0;
    }

    public void CloseShop()
    {
        shopCanvas.SetActive(false);
        Time.timeScale = 1;
    }
}