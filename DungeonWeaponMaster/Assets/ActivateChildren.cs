﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateChildren : MonoBehaviour {

	// Use this for initialization
	void Start () {

				
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            foreach (Transform child in transform)
            {
                child.gameObject.SetActive(false);
                child.gameObject.GetComponent<BoxCollider>().enabled = false;
            }
        }
    }
}
