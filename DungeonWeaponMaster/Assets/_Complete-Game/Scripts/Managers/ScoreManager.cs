﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace CompleteProject
{
    public class ScoreManager : MonoBehaviour
    {
        public static int money;        // The player's current amount of money in the wallet.


        Text text;                      // Reference to the Text component.


        void Awake ()
        {
            // Set up the reference.
            text = GetComponent <Text> ();

            // Reset the wallet.
            money = 0;
        }


        void Update ()
        {
            // Set the displayed text to be the symbol "$" followed by the amount of money.
            text.text = "$ " + money;
        }
    }
}