﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressTrigger : MonoBehaviour {

    public Slider progressSlider;

	// Use this for initialization
	void Start () {
		
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            progressSlider.value += 1;
            Destroy(gameObject);
        }
    }
}
